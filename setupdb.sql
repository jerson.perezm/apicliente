-- configuration database for this project

-- creates database monolitoApp
CREATE DATABASE IF NOT EXISTS monolitoApp;

-- creates a database user dev
CREATE USER IF NOT EXISTS 'monolitoapp_dev'@'localhost' IDENTIFIED BY '--aka su password--';

-- add privileges to the user dev
GRANT ALL PRIVILEGES ON monolitoApp.* TO 'monolitoapp_dev'@'localhost';
GRANT SELECT ON performance_schema.* TO 'monolitoapp_dev'@'localhost';
