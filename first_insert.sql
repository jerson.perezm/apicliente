INSERT INTO `tipo_identificacion` (`codigo`, `nombre`) VALUES ('CC.', 'Cedula De Ciudadania');
INSERT INTO `tipo_identificacion` (`codigo`, `nombre`) VALUES ('NIT', 'NIT');
INSERT INTO `tipo_identificacion` (`codigo`, `nombre`) VALUES ('PA.', 'Pasaporte');
INSERT INTO `tipo_identificacion` (`codigo`, `nombre`) VALUES ('CE.', 'Cédula De Extrangería');
INSERT INTO `tipo_identificacion` (`codigo`, `nombre`) VALUES ('TI.', 'Tarjeta de Identidad');
INSERT INTO `tipo_identificacion` (`codigo`, `nombre`) VALUES ('RC.', 'Registro Civil');
INSERT INTO `tipo_identificacion` (`codigo`, `nombre`) VALUES ('CD.', 'Carnet Diplomático');