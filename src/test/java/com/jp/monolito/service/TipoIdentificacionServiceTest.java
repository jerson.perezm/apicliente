package com.jp.monolito.service;

import com.jp.monolito.dto.TipoIdentificacionDTO;
import com.jp.monolito.models.TipoIdentificacion;
import com.jp.monolito.repository.TipoIdentificacionRepository;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@RequiredArgsConstructor
@ExtendWith(MockitoExtension.class)
class TipoIdentificacionServiceTest {


    private TipoIdentificacionService serviceUnderTest;

    @Mock
    private TipoIdentificacionRepository repository;

    @BeforeEach
    void setUp() {
        serviceUnderTest = new TipoIdentificacionService(repository);
    }

    @Test
    void listarTiposIdentificacion() {
        //given
        TipoIdentificacion tipoIdentificacion1 = new TipoIdentificacion();
        tipoIdentificacion1.setCodigo("AA");
        TipoIdentificacion tipoIdentificacion2 = new TipoIdentificacion();
        tipoIdentificacion1.setId(1L);
        //when
        when(repository.findAll()).thenReturn(Arrays.asList(tipoIdentificacion1, tipoIdentificacion2));
        //then
        List<TipoIdentificacionDTO> response = serviceUnderTest.listarTiposIdentificacion();
        assertEquals("AA", response.get(0).getCodigo());
        assertEquals(1L, response.get(0).getId());
    }

}