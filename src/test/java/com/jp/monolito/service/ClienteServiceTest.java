package com.jp.monolito.service;

import com.jp.monolito.dto.ClienteDTO;
import com.jp.monolito.models.Cliente;
import com.jp.monolito.models.TipoIdentificacion;
import com.jp.monolito.repository.ClienteCriteriaFilter;
import com.jp.monolito.repository.ClienteCriteriaRepository;
import com.jp.monolito.repository.ClienteRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.webjars.NotFoundException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.BDDMockito.*;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class ClienteServiceTest {

    ClienteService clienteServiceUnderTest;
    private Cliente cliente;
    private ClienteDTO clienteDto;

    @Mock
    ClienteRepository clienteRepository;
    @Mock

    ClienteCriteriaRepository clienteCriteriaRepository;


    @BeforeEach
    void setUp() {
        clienteServiceUnderTest = new ClienteService(clienteRepository, clienteCriteriaRepository);
        cliente = creaCliente();
        clienteDto = creaClienteDTO();
    }

    @Test
    void puedeListarClientes() {
        //given
        ClienteCriteriaFilter voidCriteria = new ClienteCriteriaFilter(null, null, null);
        ArrayList<Cliente> listado = new ArrayList<>();
        listado.add(cliente);

        //when
        when(clienteCriteriaRepository.findAllWithFilters(voidCriteria)).thenReturn(listado);

        //then
        List<ClienteDTO> devuelto = clienteServiceUnderTest.listaConFiltros(voidCriteria);

        verify(clienteCriteriaRepository).findAllWithFilters(voidCriteria);
        assertThat(devuelto.get(0)).usingRecursiveComparison().isEqualTo(clienteDto);
    }


    @Test
    void puedeCreaCliente() {

        //given
        //when
        when(clienteRepository.findClientesByTipoIdentificacionAndIdentificacion(
                any(),
                any()
        )).thenReturn(null);
        given(clienteRepository.save(any())).willReturn(cliente);

        clienteServiceUnderTest.crear(clienteDto);

        // then
        ArgumentCaptor<Cliente> clienteArgCaptor = ArgumentCaptor.forClass(Cliente.class);

        verify(clienteRepository).save(clienteArgCaptor.capture());

        Cliente capturedCliente = clienteArgCaptor.getValue();


        assertThat(capturedCliente).usingRecursiveComparison().isEqualTo(cliente);
    }


    @Test
    void noPuedeCreaClienteQueYaExiste() {
        /*Cuando intenta crear un cliente que yta existe, debe lanzar excepción */

        //given
        String mensaje = "Ya existe un Cliente con CC. = 123456789";
        cliente.setTipoIdentificacion(new TipoIdentificacion(1L, "CC.", ""));

        when(clienteRepository.findClientesByTipoIdentificacionAndIdentificacion(
                any(),
                any()
        )).thenReturn(cliente);
        //when
        // then
        assertThatThrownBy(() -> clienteServiceUnderTest.crear(clienteDto))
                .isInstanceOf(DuplicateKeyException.class)
                .hasMessage(mensaje);
    }

    @Test
    void puedeObtenerCliente() {
        //when
        when(clienteRepository.findById(1L)).thenReturn(Optional.of(cliente));
        clienteServiceUnderTest.obtenerPorId(1L);

        //then
        verify(clienteRepository).findById(1L);
    }

    @Test
    void noPuedeObtenerClienteQueNoExiste() {
        //when
        when(clienteRepository.findById(2L)).thenReturn(Optional.empty());

        //then
        assertThatThrownBy(() -> clienteServiceUnderTest.obtenerPorId(2L))
                .isInstanceOf(NotFoundException.class)
                .hasMessage("No se encuentra un Cliente con id=2");
    }

    @Test
    void puedeBorrarCliente() {
        clienteServiceUnderTest.borrarPorId(1L);
        verify(clienteRepository).deleteById(1L);
    }

    @Test
    void puedeEditarCliente() {

        //given
        //when
        when(clienteRepository.findById(1L)).thenReturn(Optional.of(cliente));
        when(clienteRepository.save(any())).thenReturn(cliente);

        clienteServiceUnderTest.editar(clienteDto,cliente.getId());

        // then
        ArgumentCaptor<Cliente> clienteArgCaptor = ArgumentCaptor.forClass(Cliente.class);

        verify(clienteRepository).save(clienteArgCaptor.capture());

        Cliente capturedCliente = clienteArgCaptor.getValue();

        assertThat(capturedCliente).usingRecursiveComparison().isEqualTo(cliente);
    }

    @Test
    void puedeEditarClienteSinId() {

        //given
        clienteDto.setId(null); //el dto tiene id null
        //when
        when(clienteRepository.findById(1L)).thenReturn(Optional.of(cliente));
        when(clienteRepository.save(any())).thenReturn(cliente);
        ArgumentCaptor<Cliente> clienteArgCaptor = ArgumentCaptor.forClass(Cliente.class);

        //then
        clienteServiceUnderTest.editar(clienteDto,1L);
        verify(clienteRepository).save(clienteArgCaptor.capture());
        //cliente.setId(1L);//se restablece el id del dto para compararlo

        Cliente capturedCliente = clienteArgCaptor.getValue();
        assertThat(capturedCliente).usingRecursiveComparison().isEqualTo(cliente);
    }


    @Test
    void fallaEditarClienteIdErroneo() {

        //given
        Long idBuscado = 2L; // diferente al id del dto
        //then
        assertThatThrownBy(() -> clienteServiceUnderTest.editar(clienteDto, idBuscado))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Error en el id enviado");
    }


    @Test
    void fallaEditarClienteDuplicado() {

        //given
        //when
        when(clienteRepository.findById(1L)).thenReturn(Optional.of(cliente));
        when(clienteRepository.save(any())).thenThrow(new DuplicateKeyException(""));

        //then
        assertThatThrownBy(() -> clienteServiceUnderTest.editar(clienteDto, cliente.getId()))
                .isInstanceOf(DataIntegrityViolationException.class)
                .hasMessage("Ya existe un Cliente con 1 = 123456789");
    }


    private ClienteDTO creaClienteDTO() {

        ClienteDTO nuevoCliente = new ClienteDTO();

        nuevoCliente.setNombres("Nombres");
        nuevoCliente.setApellidos("Apellidos");
        nuevoCliente.setId(1L);
        nuevoCliente.setCiudadNacimiento("Copacabana");
        nuevoCliente.setTipoIdentificacionId(1);
        nuevoCliente.setIdentificacion("123456789");
        nuevoCliente.setFechaNacimiento("2000-01-01");

        return nuevoCliente;
    }

    private Cliente creaCliente() {

        Cliente nuevoCliente = new Cliente();

        nuevoCliente.setNombres("Nombres");
        nuevoCliente.setApellidos("Apellidos");
        nuevoCliente.setId(1L);
        nuevoCliente.setCiudadNacimiento("Copacabana");

        TipoIdentificacion tipoDocument = new TipoIdentificacion();
        tipoDocument.setId(1L);
        nuevoCliente.setTipoIdentificacion(tipoDocument);
        nuevoCliente.setIdentificacion("123456789");
        try {
            nuevoCliente.setFechaNacimiento(new SimpleDateFormat("yyyy-MM-dd").parse("2000-01-01"));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return nuevoCliente;
    }
}