package com.jp.monolito.service;


import com.jp.monolito.dto.ClienteDTO;
import com.jp.monolito.models.Imagen;
import com.jp.monolito.repository.ImagenRepository;
import org.bson.BsonBinarySubType;
import org.bson.types.Binary;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.webjars.NotFoundException;

import java.io.IOException;
import java.util.Base64;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ImagenServiceTest {

    ImagenService imagenServiceUnderTest;

    @Mock
    ImagenRepository imagenRepository;

    @Mock
    ClienteService clienteService;

    private Imagen imagen;
    private final String imagenContent = "Qk06AAAAAAAAADYAAAAoAAAAAQAAAAEAAAABABgAAAAAAAQAAADEDgAAxA4AAAAAAAAAAAAAAAAAAA==";


    @BeforeEach
    void setUp() {
        imagenServiceUnderTest = new ImagenService(imagenRepository, clienteService);
        imagen = creaMockImagen();
    }

    @Test
    void puedeGuardarImagen() throws IOException {
        //given
        MockMultipartFile file = new MockMultipartFile(
                "name",
                "name",
                MediaType.IMAGE_JPEG_VALUE,
                Base64.getDecoder().decode(imagenContent)
        );
        /* mock the request context to get the url */
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        imagen.setId(null);

        ClienteDTO clienteDto = new ClienteDTO();
        clienteDto.setId(1L);

        //when
        when(clienteService.obtenerPorId(1L)).thenReturn(clienteDto);
        imagenServiceUnderTest.guardar(file, 1L);

        //then
        ArgumentCaptor<Imagen> imagenArgumentCaptorArgCaptor = ArgumentCaptor.forClass(Imagen.class);

        verify(imagenRepository).save(imagenArgumentCaptorArgCaptor.capture());

        Imagen imagenCapturada = imagenArgumentCaptorArgCaptor.getValue();

        assertThat(imagenCapturada).usingRecursiveComparison().isEqualTo(imagen);
    }

    @Test
    void noPuedeGuardarImagenVoid() {
        //given
        MockMultipartFile file = new MockMultipartFile(
                "name",
                "name",
                MediaType.IMAGE_JPEG_VALUE,
                Base64.getDecoder().decode("")
        );

        assertThatThrownBy(() -> imagenServiceUnderTest.guardar(file, 1L))
                .isInstanceOf(IOException.class)
                .hasMessage("Imagen valida o no vacía");
    }

    @Test
    void noPuedeGuardarImagenNull() {
        //given

        assertThatThrownBy(() -> imagenServiceUnderTest.guardar(null, 1L))
                .isInstanceOf(IOException.class)
                .hasMessage("Imagen valida o no vacía");
    }

    @Test
    void puedeObtenerImagenPorId() {
        //when
        when(imagenRepository.findImagenById("uuid")).thenReturn(Optional.ofNullable(imagen));
        assertThat(imagenServiceUnderTest.obtenerPorId("uuid")).usingRecursiveComparison().isEqualTo(imagen);
    }

    @Test
    void noPuedeObtenerImagenPorId() {
        //when
        when(imagenRepository.findImagenById("uuid")).thenReturn(Optional.empty());
        assertThatThrownBy(() -> imagenServiceUnderTest.obtenerPorId("uuid"))
                .isInstanceOf(NotFoundException.class)
                .hasMessage("Imagen No Encontrada");
    }

    @Test
    void puedeObtenerImagenPorIdCliente() {
        //when
        when(imagenRepository.findImagenByCliente(1L)).thenReturn(Optional.ofNullable(imagen));
        assertThat(imagenServiceUnderTest.obtenerPorClienteId(1L)).usingRecursiveComparison().isEqualTo(imagen);
    }

    @Test
    void noPuedeObtenerImagenPorIdCliente() {
        //when
        when(imagenRepository.findImagenByCliente(1L)).thenReturn(Optional.empty());
        assertThatThrownBy(() -> imagenServiceUnderTest.obtenerPorClienteId(1L))
                .isInstanceOf(NotFoundException.class)
                .hasMessage("No existe Imagen Registrada para el id=1.");
    }

    @Test
    void borrarImagen() {

        imagenRepository.deleteImagenByCliente(1L);
        //then
        ArgumentCaptor<Long> idArgumentCaptorArgCaptor = ArgumentCaptor.forClass(Long.class);
        verify(imagenRepository).deleteImagenByCliente(idArgumentCaptorArgCaptor.capture());
        Long idCapturado = idArgumentCaptorArgCaptor.getValue();

        assertThat(idCapturado).isEqualTo(1L);
    }

    Imagen creaMockImagen() {

        Imagen nuevaImagen = new Imagen();
        nuevaImagen.setId(UUID.randomUUID().toString());
        nuevaImagen.setCliente(1L);
        nuevaImagen.setFileContent(new Binary(BsonBinarySubType.BINARY, Base64.getDecoder().decode(imagenContent)));
        return nuevaImagen;
    }
}