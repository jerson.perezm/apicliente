package com.jp.monolito.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class CustomValidatorTest {

    private final CustomValidator customValidator = new CustomValidator();

    @Test
    void fechaValida() {
        assertTrue(customValidator.isValid("2020-01-01", null));
    }

    @Test
    void fechaInvalida() {
        assertFalse(customValidator.isValid("20200-01-01", null));
        assertFalse(customValidator.isValid("2020-15-01", null));
        assertFalse(customValidator.isValid("2020-10-91", null));
    }
}