package com.jp.monolito.service;

import com.jp.monolito.dto.TipoIdentificacionDTO;
import com.jp.monolito.models.TipoIdentificacion;
import com.jp.monolito.repository.TipoIdentificacionRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TipoIdentificacionService {

    private final TipoIdentificacionRepository tipoIdentificacionRepository;
    private final ModelMapper mapper = new ModelMapper();

    public List<TipoIdentificacionDTO> listarTiposIdentificacion() {
        return mapListTipoiIdentificacionToDto(tipoIdentificacionRepository.findAll());
    }

    private List<TipoIdentificacionDTO> mapListTipoiIdentificacionToDto(List<TipoIdentificacion> entityList) {
        ArrayList<TipoIdentificacionDTO> dtoList = new ArrayList<>();
        for (TipoIdentificacion entity : entityList) {
            dtoList.add(mapper.map(entity, TipoIdentificacionDTO.class));
        }
        return dtoList;
    }

}
