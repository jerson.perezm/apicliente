package com.jp.monolito.service;

import com.jp.monolito.dto.ClientMapper;
import com.jp.monolito.dto.ClienteDTO;
import com.jp.monolito.models.Cliente;
import com.jp.monolito.repository.ClienteCriteriaRepository;
import com.jp.monolito.repository.ClienteCriteriaFilter;
import com.jp.monolito.repository.ClienteRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;


import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional
public class ClienteService {

    private final ClienteRepository clienteRepository;
    private final ClienteCriteriaRepository criteriaRepository;
    private final ClientMapper mapper = new ClientMapper();


    public List<ClienteDTO> listaConFiltros(ClienteCriteriaFilter clienteCriteriaFilter) {
        return mapper.mapClientListToDto(criteriaRepository.findAllWithFilters(clienteCriteriaFilter));
    }

    public ClienteDTO crear(ClienteDTO clienteDto) {
        Cliente cliente = mapper.mapDtoToClient(clienteDto);
        Cliente clienteExistente = clienteRepository.findClientesByTipoIdentificacionAndIdentificacion(
                cliente.getTipoIdentificacion(),
                cliente.getIdentificacion()
        );

        if (clienteExistente != null) {
            throw new DuplicateKeyException(String.format("Ya existe un Cliente con %s = %s",
                    clienteExistente.getTipoIdentificacion().getCodigo(),
                    clienteExistente.getIdentificacion()
            ));
        }

        return mapper.mapClientToDto(clienteRepository.save(cliente));

    }

    public ClienteDTO obtenerPorId(Long id) {
        Optional<Cliente> cliente = clienteRepository.findById(id);
        if (!cliente.isPresent()) {
            throw new NotFoundException(String.format("No se encuentra un Cliente con id=%d", id));
        }
        return mapper.mapClientToDto(cliente.get());
    }

    public void borrarPorId(Long id) {
        clienteRepository.deleteById(id);
    }

    public ClienteDTO editar(ClienteDTO clienteDto, Long id) {
        if (clienteDto.getId() == null) {
            clienteDto.setId(id);
        } else if (!clienteDto.getId().equals(id)) {
            throw new IllegalArgumentException("Error en el id enviado");
        }
        Cliente clienteEditado = mapper.mapDtoToClient(clienteDto);
        obtenerPorId(clienteEditado.getId()); // Lanza excepción si no existe
        try {
            return mapper.mapClientToDto(
                    clienteRepository.save(clienteEditado)
            );
        } catch (DataIntegrityViolationException e) {
            throw new DuplicateKeyException(String.format("Ya existe un Cliente con %s = %s",
                    clienteEditado.getTipoIdentificacion().getId(),
                    clienteEditado.getIdentificacion()
            ));
        }

    }

}
