package com.jp.monolito.service;

import com.jp.monolito.models.Imagen;
import com.jp.monolito.repository.ImagenRepository;
import lombok.RequiredArgsConstructor;
import org.bson.BsonBinarySubType;
import org.bson.types.Binary;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.webjars.NotFoundException;

import java.io.IOException;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ImagenService {
    private final ImagenRepository imagenRepository;
    private final ClienteService clienteService;

    /**
     * guarda una imagen basado en su contenido e id del cliente relacionado
     *
     * @param file contenido de la imagen
     * @param id   clave primaria del cliente relacionado
     * @return URL del recurso creado
     * @throws IOException cuando no es posible obtener los bytes de la imagen
     */
    public Imagen guardar(MultipartFile file, Long id) throws IOException {

        if (file == null || file.getSize() == 0) {
            throw new IOException("Imagen valida o no vacía");
        }
        /* crea la entidad imagen*/
        Imagen imagenNueva = new Imagen();
        imagenNueva.setFileContent(new Binary(BsonBinarySubType.BINARY, file.getBytes()));
        imagenNueva.setCliente(id);

        /*lanza error si no existe cliente con el id dado*/
        clienteService.obtenerPorId(id);

        /* borra la imagen anterior si existe */
        Optional<Imagen> imagenAnterior = imagenRepository.findImagenByCliente(id);
        imagenAnterior.ifPresent(imagenRepository::delete);

        /* guarda la nueva imagen */
        return imagenRepository.save(imagenNueva);

    }

    /**
     * devuelve una imagen por el id de su propietario
     *
     * @param id id del Cliente del que se quiere la imagen
     * @return objeto imagen encontrada
     */
    public Imagen obtenerPorClienteId(Long id) {
        Optional<Imagen> imagen = imagenRepository.findImagenByCliente(id);
        if (!imagen.isPresent()) {
            throw new NotFoundException(String.format("No existe Imagen Registrada para el id=%d.", id));
        }
        return imagen.get();

    }

    /**
     * devuelve una imagen pos su id en mongo
     *
     * @param imagenId id del documento en mongodb
     * @return objeto imagen encontrada
     */
    public Imagen obtenerPorId(String imagenId) {
        Optional<Imagen> imagen = imagenRepository.findImagenById(imagenId);
        if (!imagen.isPresent()) {
            throw new NotFoundException("Imagen No Encontrada");
        }
        return imagen.get();
    }

    public void borrarPorClienteId(Long clienteId) {
        imagenRepository.deleteImagenByCliente(clienteId);
    }
}
