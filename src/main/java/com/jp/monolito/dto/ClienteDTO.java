package com.jp.monolito.dto;

import com.jp.monolito.util.StringDateBefore;
import lombok.Data;

import javax.validation.constraints.*;
import java.io.Serializable;

@Data
public class ClienteDTO implements Serializable {

    private Long id;

    @NotBlank(message = "Este campo no puede estar vacío")
    @Size(max = 64, message = "Cadena muy Larga")
    private String nombres;

    @NotBlank(message = "Este campo no puede estar vacío")
    @Size(max = 64, message = "Cadena muy Larga")
    private String apellidos;

    @Min(value = 1, message = "Tipo de Identificacion No Valido")
    @Max(value = 7, message = "Tipo de Identificacion No Valido")
    private Integer tipoIdentificacionId;

    @NotBlank(message = "Este campo no puede estar vacío")
    @Size(min = 4, max = 20, message = "documento No válido")
    private String identificacion;

    @NotBlank(message = "Este campo no puede estar vacío")
    @StringDateBefore(message = "Fecha No Valida")
    private String fechaNacimiento;

    @NotBlank(message = "Este campo no puede estar vacío")
    @Size(max = 128, message = "Cadena demasiado larga")
    private String ciudadNacimiento;
}
