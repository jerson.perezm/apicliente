package com.jp.monolito.dto;

import com.jp.monolito.models.Cliente;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;

import java.lang.reflect.MalformedParametersException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Define las reglas de mapeo de Cliente a ClienteDTO y viceversa
 */
public class ClientMapper {

    private final ModelMapper modelMapper;

    public ClientMapper() {

        // crea el mapper
        this.modelMapper = new ModelMapper();

        //define el mapper para pasar Date to String
        TypeMap<Cliente, ClienteDTO> stringDateMapper = this.modelMapper.createTypeMap(Cliente.class, ClienteDTO.class);
        stringDateMapper.addMappings(
                mapper -> mapper.using(dateStringConverter).map(Cliente::getFechaNacimiento, ClienteDTO::setFechaNacimiento)
        );

        //define el mapper para pasar String to Date
        TypeMap<ClienteDTO, Cliente> dateStringMapper = this.modelMapper.createTypeMap(ClienteDTO.class, Cliente.class);
        dateStringMapper.addMappings(
                mapper -> mapper.using(stringDateConverter).map(ClienteDTO::getFechaNacimiento, Cliente::setFechaNacimiento)
        );
    }


    /**
     * Mapea Clientes enClienteDTO
     *
     * @param cliente - a ser mapeado
     * @return un nuevo ClienteDTO correspondiente
     */
    public ClienteDTO mapClientToDto(Cliente cliente) {
        this.modelMapper.addConverter(dateStringConverter);
        return modelMapper.map(cliente, ClienteDTO.class);
    }

    /**
     * Mapea una lista de entidades cliente hacia una lista de ClienteDTO
     *
     * @param clientes - lista de Clientes a mapear
     * @return Lista de ClienteDTO
     */
    public List<ClienteDTO> mapClientListToDto(List<Cliente> clientes) {
        return clientes.stream().map(this::mapClientToDto).collect(Collectors.toList());
    }

    /**
     * Mapea ClienteDTO en Entidades Cliente
     *
     * @param clienteDto dto a ser mapeado
     * @return nueva entidad Cliente
     */
    public Cliente mapDtoToClient(ClienteDTO clienteDto) {

        return modelMapper.map(clienteDto, Cliente.class);
    }

    /**
     * Convertidor de fecha en formato String a Date
     */
    private final Converter<Date, String> dateStringConverter = c -> {

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(c.getSource());
    };

    /**
     * Convertidor de Date a String en formato ISO
     */
    private final Converter<String, Date> stringDateConverter = c -> {

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return df.parse(c.getSource());
        } catch (ParseException e) {
            throw new MalformedParametersException("error");
        }
    };


}
