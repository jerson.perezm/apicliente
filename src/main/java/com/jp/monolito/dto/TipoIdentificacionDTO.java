package com.jp.monolito.dto;

import lombok.Data;

@Data
public class TipoIdentificacionDTO {
    private Long id;
    private String codigo;
    private String nombre;
}
