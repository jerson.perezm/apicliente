package com.jp.monolito.handler;

import lombok.Data;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.webjars.NotFoundException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@ControllerAdvice
public class ApiExceptionHandler {


    @ExceptionHandler(value = {MethodArgumentNotValidException.class})
    protected ResponseEntity<Object> handleValidationException(Exception exception) {
        HttpStatus status = BAD_REQUEST;
        return ResponseEntity.status(status).body(
                buildErrorResponse(
                        status,
                        "Error de validación de datos",
                        ((MethodArgumentNotValidException) exception).getBindingResult().getFieldErrors(),
                        ""
                )
        );
    }

    @ExceptionHandler(value = {DuplicateKeyException.class, NotFoundException.class, Exception.class})
    protected ResponseEntity<Object> handleConflict(Exception exception) {
        HttpStatus status = exception.getClass() == NotFoundException.class ? NOT_FOUND : BAD_REQUEST;
        exception.printStackTrace();
        return ResponseEntity.status(status).body(
                buildErrorResponse(
                        status,
                        exception.getClass().getSimpleName(),
                        null,
                        exception.getLocalizedMessage()
                ));
    }

    private HashMap<String, Object> buildErrorResponse(HttpStatus status, String message, List<FieldError> fieldErrors, String description) {
        ErrorBody errorBody = new ErrorBody(status.value(), message);

        if (fieldErrors != null) {
            for (FieldError fieldError : fieldErrors) {
                errorBody.addFieldError(fieldError.getField(), fieldError.getDefaultMessage());
            }
        }

        errorBody.setDescription(description);

        HashMap<String, Object> error = new HashMap<>(1);
        error.put("error", errorBody);
        return error;
    }


    @Data
    static class ErrorBody {
        private final int status;
        private final String message;
        private String description;
        private List<ErrorField> errors = new ArrayList<>();

        public void addFieldError(String path, String message) {
            errors.add(new ErrorField(path, message));
        }

    }

    @Data
    static class ErrorField {
        private final String field;
        private final String message;
    }

}
