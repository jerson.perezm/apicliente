package com.jp.monolito.controller;

import com.jp.monolito.models.Imagen;
import com.jp.monolito.service.ImagenService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.net.URI;

@RestController
@RequiredArgsConstructor
@RequestMapping("/clientes")
public class ImagenController {

    private final ImagenService imagenService;

    @GetMapping(path = "/{cliente_id}/imagen")
    @ResponseBody
    public ResponseEntity<Object> obtenerImagen(@PathVariable("cliente_id") Long id) {
        Imagen imagen = imagenService.obtenerPorClienteId(id);
        return ResponseEntity.ok()
                .contentLength(imagen.getFileContent().length())
                .contentType(MediaType.valueOf(MediaType.IMAGE_JPEG_VALUE))
                .body(imagen.getFileContent().getData());
    }

    @GetMapping(path = "/imagenes/{imagen_id}")
    @ResponseBody
    public ResponseEntity<Object> obtenerImagenPorId(@PathVariable("imagen_id") String id) {
        Imagen imagen = imagenService.obtenerPorId(id);
        return ResponseEntity.ok()
                .contentLength(imagen.getFileContent().length())
                .contentType(MediaType.valueOf(MediaType.IMAGE_JPEG_VALUE))
                .body(imagen.getFileContent().getData());
    }

    @PostMapping(path = "/{cliente_id}/imagen", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Object> crearImagen(@PathVariable("cliente_id") Long clienteId, @RequestBody MultipartFile file) throws IOException {
        Imagen nuevaImagen = imagenService.guardar(file, clienteId);
        URI uri = URI.create(
                ServletUriComponentsBuilder
                        .fromCurrentContextPath()
                        .path(String.format("/imagenes/%s", nuevaImagen.getId()))
                        .toUriString()
        );
        return ResponseEntity.created(uri).build();
    }

    @DeleteMapping(path = "/{cliente_id}/imagen")
    public ResponseEntity<Object> borrarImagen(@PathVariable("cliente_id") Long id) {
        imagenService.borrarPorClienteId(id);
        return ResponseEntity.noContent().build();
    }

}
