package com.jp.monolito.controller;


import com.jp.monolito.dto.TipoIdentificacionDTO;
import com.jp.monolito.service.TipoIdentificacionService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/TiposIdentificacion")
public class TipoIdentificacionController {
    private final TipoIdentificacionService tipoIdentificacionService;


    @GetMapping(path = "")
    public ResponseEntity<List<TipoIdentificacionDTO>> listarTiposDeIdentificacion() {
        return ResponseEntity.ok().body(
                tipoIdentificacionService.listarTiposIdentificacion()
        );
    }
}
