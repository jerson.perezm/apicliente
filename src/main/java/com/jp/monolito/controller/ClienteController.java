package com.jp.monolito.controller;

import com.jp.monolito.dto.ClienteDTO;
import com.jp.monolito.repository.ClienteCriteriaFilter;
import com.jp.monolito.service.ClienteService;
import com.jp.monolito.service.ImagenService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping(path = "/clientes")
@Validated
public class ClienteController {

    private final ClienteService clienteService;
    private final ImagenService imagenService;

    @GetMapping(path = "/")
    public ResponseEntity<List<ClienteDTO>> listarClientes(
            @RequestParam(name = "tipo_id", required = false) Integer tipoIdentificacion,
            @RequestParam(name = "identificacion", required = false) String identificacion,
            @RequestParam(name = "edad>=", required = false) Integer mayorOIgualA
    ) {
        return ResponseEntity.ok().body(
                clienteService.listaConFiltros(
                        new ClienteCriteriaFilter(tipoIdentificacion, identificacion, mayorOIgualA)
                )
        );
    }

    @PostMapping(path = "/")
    public ResponseEntity<ClienteDTO> crearCliente(@RequestBody @Valid ClienteDTO clienteDto) {
        ClienteDTO nuevoCliente = clienteService.crear(clienteDto);
        URI uri = URI.create(
                ServletUriComponentsBuilder
                        .fromCurrentContextPath()
                        .path(String.format("/clientes/%d", nuevoCliente.getId()))
                        .toUriString()
        );
        return ResponseEntity.created(uri).body(nuevoCliente);
    }

    @GetMapping(path = "/{cliente_id}")
    public ResponseEntity<ClienteDTO> obtenerCliente(@PathVariable("cliente_id") Long id) {
        return ResponseEntity.ok().body(
                clienteService.obtenerPorId(id)
        );
    }

    @PutMapping(path = "/{cliente_id}")
    public ResponseEntity<ClienteDTO> actualizarCliente(@PathVariable("cliente_id") Long id, @RequestBody ClienteDTO clienteDto) {
        return ResponseEntity.ok().body(
                clienteService.editar(clienteDto, id)
        );
    }

    @DeleteMapping(path = "/{cliente_id}")
    public ResponseEntity<Object> borrarCliente(@PathVariable("cliente_id") Long id) {
        clienteService.borrarPorId(id);
        imagenService.borrarPorClienteId(id);
        return ResponseEntity.noContent().build();
    }



}
