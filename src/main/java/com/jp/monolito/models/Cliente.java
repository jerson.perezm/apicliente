package com.jp.monolito.models;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

import static javax.persistence.TemporalType.DATE;

@Setter
@Getter
@RequiredArgsConstructor
@Entity
@Table(
        uniqueConstraints = @UniqueConstraint(
                columnNames = {"tipo_identificacion_id", "identificacion"}
        )
)
public class Cliente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 64, nullable = false)
    private String nombres;

    @Column(length = 64, nullable = false)
    private String apellidos;

    @ManyToOne(optional = false, cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
    @JoinColumn(name = "tipo_identificacion_id", nullable = false)
    private TipoIdentificacion tipoIdentificacion;

    @Column(nullable = false)
    private String identificacion;

    @Temporal(value = DATE)
    private Date fechaNacimiento;

    @Column(length = 128)
    private String ciudadNacimiento;

}
