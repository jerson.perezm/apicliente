package com.jp.monolito.models;


import lombok.*;
import org.bson.types.Binary;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document
@Getter
@Setter
@RequiredArgsConstructor
public class Imagen {

    @Id
    private String id;

    private Long cliente;

    private Binary fileContent;
}
