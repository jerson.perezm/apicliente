package com.jp.monolito.repository;

import com.jp.monolito.models.TipoIdentificacion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TipoIdentificacionRepository extends JpaRepository <TipoIdentificacion, Long> {

}
