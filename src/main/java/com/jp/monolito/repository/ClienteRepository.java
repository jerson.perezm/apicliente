package com.jp.monolito.repository;

import com.jp.monolito.models.Cliente;
import com.jp.monolito.models.TipoIdentificacion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long> {

    Cliente findClientesByTipoIdentificacionAndIdentificacion(TipoIdentificacion tipoIdentificacion, String identification);

}
