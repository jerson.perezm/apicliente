package com.jp.monolito.repository;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ClienteCriteriaFilter {
    private Integer tipoIdentificacion;
    private String identificacion;
    private Integer mayorOIgualA;
}
