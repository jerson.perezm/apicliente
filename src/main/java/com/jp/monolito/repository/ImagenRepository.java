package com.jp.monolito.repository;

import com.jp.monolito.models.Imagen;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface ImagenRepository extends MongoRepository<Imagen, String> {

    Optional<Imagen> findImagenByCliente(Long id);

    Optional<Imagen> findImagenById(String id);

    void deleteImagenByCliente(long id);
}
