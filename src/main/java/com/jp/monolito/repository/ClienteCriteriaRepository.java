package com.jp.monolito.repository;

import com.jp.monolito.models.Cliente;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.*;

@Repository
public class ClienteCriteriaRepository {
    private final EntityManager entityManager;
    private final CriteriaBuilder criteriaBuilder;

    public ClienteCriteriaRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
        this.criteriaBuilder = entityManager.getCriteriaBuilder();
    }


    /**
     * devuelve el listado de todos los Clientes filtrados de acuerdo a clienteCriteriaFilter
     *
     * @param clienteCriteriaFilter, objeto con los parámetros de filtrado enviados desde el cliente
     * @return listado de clientes filtrado
     */
    public List<Cliente> findAllWithFilters(ClienteCriteriaFilter clienteCriteriaFilter) {
        CriteriaQuery<Cliente> criteriaQuery = criteriaBuilder.createQuery(Cliente.class);
        Root<Cliente> clienteRoot = criteriaQuery.from(Cliente.class);
        Predicate predicate = getPredicate(clienteCriteriaFilter, clienteRoot);
        criteriaQuery.where(predicate);
        TypedQuery<Cliente> typedQuery = entityManager.createQuery(criteriaQuery);
        return typedQuery.getResultList();
    }

    /**
     * Obtiene el criterio de búsqueda de acuerdo a los filtros enviados por el usuario
     *
     * @param clienteCriteriaFilter - Objeto con la information de filtrado enviada por el usuario
     * @param clienteRoot           - Base para crear el criterio de búsqueda
     * @return predicado con el criterio de búsqueda obtenido
     */
    private Predicate getPredicate(ClienteCriteriaFilter clienteCriteriaFilter, Root<Cliente> clienteRoot) {
        List<Predicate> predicates = new ArrayList<>();

        /* agrega el filtro por tipo de documento, solo si fue solicitado */
        if (Objects.nonNull(clienteCriteriaFilter.getTipoIdentificacion())) {
            predicates.add(criteriaBuilder.equal(clienteRoot.get("tipoIdentificacion"), clienteCriteriaFilter.getTipoIdentificacion()));
        }

        /* agrega el filtro por documento, solo si fue solicitado */
        if (Objects.nonNull(clienteCriteriaFilter.getIdentificacion())) {
            predicates.add(criteriaBuilder.equal(clienteRoot.get("identificacion"), clienteCriteriaFilter.getIdentificacion()));
        }

        /* agrega el filtro por edad, solo si fue solicitado */
        if (Objects.nonNull(clienteCriteriaFilter.getMayorOIgualA())) {
            /* Obtiene la fecha actual */
            Calendar searchedDate = Calendar.getInstance();

            /* le resta la cantidad de años - 1, ya que el criterio es mayor de */
            searchedDate.set(Calendar.YEAR, searchedDate.get(Calendar.YEAR) - clienteCriteriaFilter.getMayorOIgualA());

            /* la búsqueda se hace respecto a la fecha obtenida */
            predicates.add(criteriaBuilder.lessThanOrEqualTo(clienteRoot.get("fechaNacimiento"), searchedDate.getTime()));
        }
        return criteriaBuilder.and(predicates.toArray(new Predicate[0]));

    }
}
